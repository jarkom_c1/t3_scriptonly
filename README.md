# Socket Programming

Kelompok C1:

- Naufal Alauddin Hilmi
- Qadzif Kamil Zahari
- Salman Ahmad Nurhoiriza



## Fitur Program

- Suatu master node (PC/Laptop) yang menerima perintah/job dari user.
- Satu atau lebih worker node yang dapat menerima perintah/job dari master node.
- Komunikasi antara master node dan worker node dengan menggunakan socket.
- Memonitor availabilitas dari setiap worker node(s).
- Memonitor status jobs yang sedang dieksekusi di worker node(s).
- Memonitor di worker node(s) mana suatu job dieksekusi.
- Antrian jobs pada master node berdasarkan algoritma FCFS.



## Prasyarat

1. Clone repository [ini](https://gitlab.com/jarkom_c1/t3_scriptonly). 
2. Buat satu atau lebih instance (worker node) di AWS. Pada bagian “Configure Security Groups”, pastikan untuk membuat Security Group yang baru dan klik “Add Rule”. Tambahkan rule baru dengan **type** **“All Traffic”** dan **source “0.0.0.0/0”**.
3. Pindahkan file `server.py` ke instance tersebut. 
4. Connect ke instance tersebut.
5. Pada shell, ketik `sudo yum install python3` dan tekan `Enter` untuk menginstall Python versi 3.
6. Jalankan file `server.py` di instance dengan  `python3 server.py`.
7. Server sudah siap menerima perintah dari master node (client).



## Penggunaan

1. Buat program python yang akan dijalankan di worker node.
2. Simpan file tersebut satu directory dengan `client.py`.
3. Masukkan IPv4 address setiap worker node ke dalam `hosts.txt`.
4. Jalankan `client.py`.
5. User dapat memilih untuk melakukan empat hal dengan meng-input:
   - 0 : Exit program.
   - 1 : Check availabilitas worker node(s).
   - 2 : Mengeksekusi code di worker node. Akan ada prompt untuk memasukkan nama file.
   - 3 : Check job status jobs.