import socket, threading, time
from _thread import *
from hashlib import sha256
from queue import Queue
import logging
# import threading

# host_lock = threading.Lock()
hosts = {}
jobs = {}
jobs_queue = Queue()
host_queue = Queue()
port = 8080
running = True
dashes = "---- ATTENTION HERE!!!! ----"

def debug_message(msg):
    logging.debug(msg)

def debug_from_server(ip, message):
    debug_message('{} : {}'.format(ip, message))


def print_from_server(ip, message):
    print('{} : {}'.format(ip, message))


def check_avail():
    for key, value in hosts.items():
        # Connect to server
        try:
            clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print_from_server(key, "Connecting to {}!".format(key))
            clientsocket.connect((key, port))

            # Server sends "Welcome to server!"
            data = clientsocket.recv(1024).decode('utf-8')
            print_from_server(key, data)

            # We send a check availabillity message to server
            clientsocket.send(bytes('check_avail', 'utf-8'))

            while True:
                data = clientsocket.recv(1024).decode('utf-8')
                if not data:
                    break
                print_from_server(key, data)

        except(OSError):
            print_from_server(key, "{} is dead.".format(key))
            hosts[key] = 'Dead'
            continue


def check_jobs():
    if not jobs:
        print('No jobs has been assigned')
    else:
        for key, val in jobs.items():
            print('1. {:>25}: {}'.format(key, val))


def generate_job_name(file_name):
    return 'Jobs#{} (Executing {})'.format(
        sha256(str.encode(file_name + str(time.time()))).hexdigest()[0:4],
        file_name
    )


def exec_code(exec_file_name):
    # Read exec file content
    exec_file = open(exec_file_name, 'rb')
    exec_file_content = exec_file.read()
    exec_file.close()

    global jobs
    # Insert job to queue
    push_job(
            exec_file_content,
            generate_job_name(exec_file_name)
            )


def send_job_to_server(exec_file_content, job_name, available_host):
    # Create new socket object to connect to the server (host)
    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print_from_server(available_host, 'Connecting to ' + available_host + "!")
    clientsocket.connect((available_host, port))

    # Waiting for server "Welcome" message
    data = clientsocket.recv(1024).decode('utf-8')
    print_from_server(available_host, data)

    # Notify server that we want to execute code
    clientsocket.send(bytes('exec_code', 'utf-8'))

    # Send .py file in binary to server (host)
    print("Sending jobs {} to {}!".format(job_name, available_host))
    clientsocket.send(exec_file_content)

    jobs[job_name] = 'Running on {}'.format(available_host)

    # Waiting for output of program from server
    while True:
        data = clientsocket.recv(1024).decode('utf-8')
        if not data:
            break
        print_from_server(available_host, data)

    # Make host available again after output received
    hosts[available_host] = 'Available'
    host_queue.put(available_host)
    # host_lock.release()

    print_from_server(available_host, 'Done! Terminating connection!')
    jobs[job_name] = 'Done'


def job_manager_thread():
    while running:
        # Wait until there is an available job
        job = jobs_queue.get()

        # Wait until there is an available host
        host = host_queue.get()
        while 'Die' not in [job, host] and hosts[host] != 'Available':
            host_queue.put(host)
            host = host_queue.get()

        # Stop thread if asked
        if 'Die' in [job, host]:
            break

        # Send job to server
        start_new_thread(send_job_to_server, (
            job['command'], job['name'], host,
        ))


def terminate_job_manager():
    global running
    running = False
    jobs_queue.put('Die')
    host_queue.put('Die')


def push_job(job, name):
    print(f'Insert job at queue with name {name}')
    jobs_queue.put({
        'command': job,
        'name': name,
        })
    jobs[name] = 'Waiting at queue'

def Main():
    # Set up logger
    logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG)

    # Listing server available_hostv4 Public address
    print("Reading hosts.txt!")
    host_file = open('hosts.txt', 'r')
    host_file_list = host_file.read().split()
    host_file.close()

    for host in host_file_list:
        hosts[host] = 'Available'

    print("Checking host availability!")
    try:
        check_avail()
        for host, status in hosts.items():
            if status == 'Available':
                host_queue.put(host)
        start_new_thread(job_manager_thread, ())
    except Exception as e:
        print(f'Error \n{e}\n')
        print("Error occurred. Exiting!")
        exit()

    # Start main program
    while True:
        # Ask what user wants to do
        print_command_menu()

        try:
            user_input = int(input())
        except ValueError:
            print('Invalid input value')
            continue

        if not user_input:
            print('Program will terminate.')
            terminate_job_manager()
            exit()
        elif user_input == 1:
            start_new_thread(check_avail, ())
        elif user_input == 2:
            # Ask for .py file name to user (Make sure file is
            # in same directory as this scravailable_hostt)
            exec_file_name = input('\n{}\n{}\n{}\n\n'.format(
                dashes,
                'Enter file name to execute in the server (e.g. python.py):',
                dashes
                )
            )
            # start_new_thread(exec_code, (exec_file_name,))
            exec_code(exec_file_name)
        elif user_input == 3:
            check_jobs()


def print_command_menu():
    print('\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n'.format(
        dashes, 
        "What would you like to do? (Enter number)",
        '0. exit',
        '1. Check worker node availabillity!',
        '2. Execute code in server!',
        '3. Check jobs condition',
        'Action choice (Enter 0 or 1):',
        dashes
        )
    )


if __name__ == '__main__':
    Main()
