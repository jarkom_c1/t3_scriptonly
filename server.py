import os, socket, threading
from _thread import *


# Function for checking the availability of this server
def check_avail(clientsocket, address, availability):
    clientsocket.send(bytes("I am {}.".format(availability), 'utf-8'))
    clientsocket.send(bytes("Closing connection!", 'utf-8'))
    clientsocket.close()
    print("Connection to {} closed.".format(address))


# Function to execute code from client
def exec_code(clientsocket, address, status):
    # Menerima file dari client
    f = open('rec_file.py', 'wb')
    l = clientsocket.recv(4096)
    clientsocket.send(bytes("File received by server!", "utf-8"))
    f.write(l)
    f.close()

    clientsocket.send(bytes("Output of file is: ", 'utf-8'))

    # Execute file dari client
    os.system('python3 rec_file.py > output.txt')

    f = open('output.txt', 'rb')
    l = f.read()
    # Send output di sini
    clientsocket.send(l)
    f.close()

    # Cleanup
    if (os.path.exists('rec_file.py')):
        os.remove('rec_file.py')

    if (os.path.exists('output.txt')):
        os.remove('output.txt')

    # Set back availability ot active
    status['availability'] = 'active'

    print('Closing connection to client!')
    clientsocket.close()
    print('Close connection to {} successful!'.format(address))


def main():
    # Koneksi dan konfigurasi socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    host = socket.gethostname()
    port = 8080
    status = {
        'availability':'active'
    }
    server_socket.bind((host, port))
    print("Socket binded to {}.".format(host))

    # Banyaknya connection yang dapat diterima
    server_socket.listen(5)

    while True:
        print("Listening to port {}.".format(port))
        clientsocket, address = server_socket.accept()
        print("Connection from {} has been established!".format(address))

        msg = "Welcome to the server!"
        clientsocket.send(bytes(msg, "utf-8"))

        # Check what user wants to do (check_avail or exec_code)
        data = clientsocket.recv(1024).decode('utf-8')
        if data == 'check_avail':
            start_new_thread(
                check_avail,
                (
                    clientsocket, 
                    address, 
                    status.get('availability')
                )
            )
        elif data == 'exec_code':
            status['availability'] = 'running'
            start_new_thread(exec_code, (clientsocket, address, status))


if __name__ == '__main__':
    main()